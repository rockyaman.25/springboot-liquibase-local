//package com.example.postgresdemo.controller;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import com.example.postgresdemo.model.Question;
//import com.example.postgresdemo.repository.QuestionRepository;
//import com.example.postgresdemo.repository.UserRepository;
//import com.example.postgresdemo.service.QuestionService;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
////import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.GetMapping;
////import org.zalando.problem.ProblemModule;
////import org.zalando.problem.violations.ConstraintViolationProblemModule;
//
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.BDDMockito.given;
//import static org.mockito.Mockito.doNothing;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
////@RunWith(SpringRunner.class)
//@WebMvcTest(QuestionController.class)
//class QuestionControllerTest {
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private TestEntityManager entityManager;
//
//    @MockBean
//    private QuestionRepository questionRepository;
//
////    @Autowired
////    private ObjectMapper objectMapper;
//
//    private List<Question> questionList;
//
//    @BeforeEach
//    void setUp(){
//        this.questionList = new ArrayList<>();
//        this.questionList.add(new Question(1L, 11L,"title1", "description1"));
//   //     this.questionList.add(new Question(2L, 22L, "title2", "description2"));
//   //     this.questionList.add(new Question(3L, 33L, "title3", "description3"));
//
////        objectMapper.registerModule(new ProblemModule());
////        objectMapper.registerModule(new ConstraintViolationProblemModule());
//
//    }
//
//    @AfterEach
//    void teajnjrDown() {
//
//    }
//
//    @Test
//    void postQuestions() throws Exception{
//        Question question = new Question(1L, 11L,"title1", "description1");
//        given(questionRepository.save(any(Question.class))).willReturn(question);
//
//        this.mockMvc.perform(post("/questions",question))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.size()", is(questionList.size())));
//
//    }
//
//    @Test
//    void createQuestion() {
//    }
//
//    @Test
//    void updateQuestion() {
//    }
//
////    @Test
////    void deleteQuestion() {
////        Long userId = 1L;
////        Question question = new Question(1L, 11L, "pwd", "Name");
////
////        given(questionRepository.findById(Long.valueOf(1))).willReturn(Optional.of(question));
////        doNothing().when(questionRepository).deleteById(question.getId());
////
////        this.mockMvc.perform(delete("/questions/{id}", question.getId()))
////                .andExpect(status().isOk())
////                .andExpect(jsonPath("$.title", is(question.getTitle())))
////                .andExpect(jsonPath("$.description", is(question.getDescription())));
////
////    }
//
//
////    @Test
////    void getQuestionByUserModelId() {
////        final Long userId = 1L;
////        final Question question = new Question(1L, 11L,"title1","description1");
////        given(questionRepository.findByUserId(userId)).willReturn(Optional.of());
////
////        this.mockMvc.perform(get("/user_model/{userId}/questions", userId))
////                .andExpect(status().isNotFound());
////    }
//
//    @Test
//    public void testSaveQuestion() throws Exception {
//
//        Question mockQuestion = new Question();
//        mockQuestion.setId(1L);
//        mockQuestion.setUserId(11L);
//        mockQuestion.setTitle("Title1");
//        mockQuestion.setDescription("Description1");
//
//        List<Question> questionList = new ArrayList<>();
//        questionList.add(mockQuestion);
//
//        Mockito.when(questionRepository.findAll(Pageable.ofSize(1))).thenReturn((Page<Question>) questionList);
//
////        entityManager.persist(mockQuestion);
////
////        Iterable<Question> allQuestionsFromDb = questionRepository.findAll();
////
////        for (Question question : allQuestionsFromDb) {
////            questionList.add(question);
////        }
////        assertThat(questionList.size()).isEqualTo(1);
//
//    }
////
////    private String mapToJson(Object object) throws JsonProcessingException {
////        ObjectMapper objectMapper = new ObjectMapper();
////        return objectMapper.writeValueAsString(object);
////    }
////
////    @CrossOrigin
////    @GetMapping(value="/alltickets",produces= MediaType.APPLICATION_JSON_VALUE)
////    public Iterable<Ticket> getAllBookedTickets(){
////        return ticketBookingService.getAllBookedTickets();
////    }
//
//}