package com.example.postgresdemo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "user_model")
public class UserModel {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @JsonIgnore
    @ApiModelProperty(notes = "The auto-generated User ID")
    @Column(name="id")
    private String id;

    @NotBlank(message = "Name is mandatory")
    @Size(min = 3, max = 20, message = "Name must be minimum 3 characters")
    @Column(name = "user_name", nullable = false)
    @ApiModelProperty(notes = "Entry of Name")
    private String name;

    @NotBlank(message = "Address is mandatory")
    @Size(min = 3, max = 100, message = "Address must be minimum 3 characters")
    @Column(name = "user_address", nullable = false)
    @ApiModelProperty(notes = "address of user")
    private String address;

    @Column(name = "user_dob", nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(notes = "Date of Birth in format YYYY-MM-DD")
    private java.sql.Date dob;

    public UserModel() {
    }

    public UserModel(String name, String address, Date dob) {
        this.name = name;
        this.address = address;
        this.dob = dob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }


    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", dob=" + dob +
                '}';
    }
}
