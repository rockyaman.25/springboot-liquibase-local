package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Answer;
import com.example.postgresdemo.model.Question;
import com.example.postgresdemo.model.UserModel;
import com.example.postgresdemo.repository.AnswerRepository;
import com.example.postgresdemo.repository.QuestionRepository;
import com.example.postgresdemo.repository.UserRepository;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @GetMapping("/user_model")
    public Page<UserModel> getUserModel(Pageable pageable) {
        return userRepository.findAll(pageable);
    }


    @PostMapping("/user_model")
    public UserModel createUserModel(@Valid @RequestBody UserModel userModel) {
        return userRepository.save(userModel);
    }

    @PutMapping("/user_model/{userId}")
    public UserModel updateUserModel(@PathVariable String userId,
                                     @Valid @RequestBody UserModel userModelRequest) {
        return userRepository.findById(userId)
                .map(userModel -> {
                    userModel.setName(userModelRequest.getName());
                    userModel.setAddress(userModelRequest.getAddress());
                    userModel.setDob(userModelRequest.getDob());
                    return userRepository.save(userModel);
                }).orElseThrow(() -> new ResourceNotFoundException("User not found with id " + userId));
    }

    @DeleteMapping("/user_model/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable String userId) {
        return userRepository.findById(userId)
                .map(user -> {
                    userRepository.delete(user);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("User not found with id " + userId));
    }

    @GetMapping("/user_model/{name}")
    public List getByName(@PathVariable String name) {
        List<UserModel> userModelList = userRepository.findByName(name);
        if (userModelList.isEmpty()) {
            throw new ResourceNotFoundException("User not found with name " + name);
        }
        Question question = questionRepository.findByUserId(userModelList.get(0).getId());
        List<Answer> answerList = answerRepository.findByQuestionId(question.getId());
        String mpCryptoPassword = "BornToFight";
        StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
        decryptor.setPassword(mpCryptoPassword);

        List output = new ArrayList();

        for(Answer answer: answerList){
            Map obj = new HashMap();
            obj.put("answer", decryptor.decrypt(answer.getText()));
            obj.put("question",answer.getQuestion().getTitle());
            obj.put("userId",question.getUserId());
            output.add(obj);
        }
        return output;
    }
}

