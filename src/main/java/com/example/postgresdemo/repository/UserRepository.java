package com.example.postgresdemo.repository;

import com.example.postgresdemo.model.UserModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserModel, String> {
//    Page<UserModel> findById(Long userId);

//    Page<UserModel> findById(Long userId, Pageable pageable);
//Page<UserModel> findById(String userId);

    List<UserModel> findByName(String name);
//    boolean existsByName(String name);

}
